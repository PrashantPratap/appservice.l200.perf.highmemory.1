﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApp.Models
{
    public class HMModel
    {
        static List<HMModel> models = new List<HMModel>();
        static Random r = new Random();
        public static void HM()
        {
            int count = r.Next(10, 100);
            for (int i=0;i<count;i++)
            {
                HMModel m = new HMModel();
                m.AllocString(count);
                models.Add(m);
            }
        }

        List<HMString> myString = new List<HMString>();
        void AllocString(int count)
        {
            for (int i = 0; i < count; i++)
            {
                myString.Add(new HMString(new string('a', r.Next(10000, 100000))));
            }
        }

        List<HMXml> myXml = new List<HMXml>();
        void AllocXml(int count)
        {
            for (int i = 0; i < count; i++)
            {
                myXml.Add(new HMXml());
            }
        }
    }
}