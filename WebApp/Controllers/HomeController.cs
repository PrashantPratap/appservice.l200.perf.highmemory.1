﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApp.Models;

namespace WebApp.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult HM()
        {
            HMXml.filepath = Server.MapPath("~\\Content\\XMLFile1.xml");
            ViewBag.init = System.GC.GetTotalMemory(false) / (1024*1024);
            // HMXml x = new HMXml();
            HMModel.HM();
            ViewBag.finial = System.GC.GetTotalMemory(false) / (1024 * 1024);
            return View();
        }
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}